#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from ASUS_I01WD device
$(call inherit-product, device/asus/ASUS_I01WD/device.mk)

PRODUCT_DEVICE := ASUS_I01WD
PRODUCT_NAME := omni_ASUS_I01WD
PRODUCT_BRAND := asus
PRODUCT_MODEL := ASUS_I01WD
PRODUCT_MANUFACTURER := asus

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="omni_zenfone6-user 13 TQ2A.230505.002.A1 27 release-keys"

BUILD_FINGERPRINT := asus/WW_I01WD/ASUS_I01WD:11/RKQ1.200710.002/18.0610.2106.156-0:user/release-keys
