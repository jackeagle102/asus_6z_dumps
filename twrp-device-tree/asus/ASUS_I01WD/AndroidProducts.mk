#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_ASUS_I01WD.mk

COMMON_LUNCH_CHOICES := \
    omni_ASUS_I01WD-user \
    omni_ASUS_I01WD-userdebug \
    omni_ASUS_I01WD-eng
